<!DOCTYPE html>
<html>
   <?php include 'head.php'?>
   <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">
         <?php include 'menu.php' ?>
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Page Header
      <small>Optional description</small>
   </h1>
</section>
<!-- Main content -->
<section class="content container-fluid">

</section>
<!-- /.content -->
         </div>
         <?php include 'footer.php'?>
      </div>
      <?php include 'js.php'?>
   </body>
</html>