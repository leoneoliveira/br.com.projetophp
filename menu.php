<!-- Main Header -->
<header class="main-header">
   <!-- Logo -->
   <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
   </a>
   <!-- Header Navbar -->
   <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">
         </ul>
      </div>
   </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
         <div class="pull-left image">
            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
         </div>
         <div class="pull-left info">
            <br>
            <p>Usuario Padrão</p>
         </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
         <br>	  
         <!-- Optionally, you can add icons to the links -->
         <li class="active"><a href="index.php"><i class="fa fa-link"></i> <span>Lista de Usuario</span></a></li>
         <li><a href="m_cadasatro.php"><i class="fa fa-link"></i> <span>Cadastro de Usuario</span></a></li>
         <li><a href="#"><i class="fa fa-link"></i> <span>Lista de Grupo</span></a></li>
         <li><a href="#"><i class="fa fa-link"></i> <span>Cadastro de grupo</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
   </section>
   <!-- /.sidebar -->
</aside>